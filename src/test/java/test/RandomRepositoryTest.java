package test;

import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@ContextConfiguration
@TestExecutionListeners(inheritListeners = false, listeners = {DependencyInjectionTestExecutionListener.class})
public class RandomRepositoryTest extends AbstractTestNGSpringContextTests {

    private static RandomRepository underlyingMock = mock(RandomRepository.class);


    @Repository
    class RandomRepository {

        /**
         * Cacheable with the argument check in condition doesn't work. Error is:
         * Method call: Attempted to call method length() on null context object
         */
        //@Cacheable(value = "next", condition = "#base.length() > 2")
        @Cacheable(value = "next")
        int getHashCode(String base) {
            Random random = new Random();
            return base.hashCode() + random.nextInt();
        }
    }

    @Configuration
    @EnableCaching(proxyTargetClass = true)
    static class Config {

        // Simulating your caching configuration
        @Bean
        CacheManager cacheManager() {
            return new ConcurrentMapCacheManager("next");
        }

        // A repository mock instead of the real proxy
        @Bean
        RandomRepository myRandomRepo() {
            return underlyingMock;
        }
    }

    @Autowired
    RandomRepository repository;

    @BeforeMethod
    public void resetMocks() {
        reset(underlyingMock);
    }

    @Test
    public void nextTest_SimpleCase() {
        int expectedCachedValue = 10;
        when(underlyingMock.getHashCode(eq("test"))).thenReturn(expectedCachedValue, 13424, 324324);

        int firstInvocationResult = repository.getHashCode("test");
        int secondInvocationResult = repository.getHashCode("test");

        assertEquals(firstInvocationResult, expectedCachedValue);
        assertEquals(secondInvocationResult, firstInvocationResult);
    }

    @Test
    public void nextTest_WithDifferentArguments() {
        int expectedCachedValueForFoo = 10;
        int expectedCachedValueForBar = 999;
        when(underlyingMock.getHashCode(eq("foo"))).thenReturn(expectedCachedValueForFoo, 21324);
        when(underlyingMock.getHashCode(eq("bar"))).thenReturn(expectedCachedValueForBar, 12213);

        int fooFirstInvocationResult = repository.getHashCode("foo");
        int barFirstInvocationResult = repository.getHashCode("bar");

        int fooSecondInvocationResult = repository.getHashCode("foo");
        int barSecondInvocationResult = repository.getHashCode("bar");

        assertEquals(fooFirstInvocationResult, expectedCachedValueForFoo);
        assertEquals(barFirstInvocationResult, expectedCachedValueForBar);

        assertEquals(fooSecondInvocationResult, fooFirstInvocationResult);
        assertEquals(barSecondInvocationResult, barFirstInvocationResult);
    }
}
